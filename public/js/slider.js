var SliderData = /** @class */ (function () {
    function SliderData() {}
    SliderData.getImgs = function () {
        return this.IMGS;
    };
    SliderData.getUrls = function () {
        return this.URLS;
    };
    SliderData.IMGS = [
        './public/images/kitten_1_mini.jpg',
        './public/images/kitten_2_mini.jpg',
        './public/images/kitten_3_mini.jpg',
        './public/images/kitten_4_mini.jpg',
        './public/images/kitten_5_mini.jpg',
    ];
    SliderData.URLS = [
        'www.mipage.com/page-1',
        'www.mipage.com/page-2',
        'www.mipage.com/page-3',
        'www.mipage.com/page-4',
        'www.mipage.com/page-5',
    ];
    return SliderData;
}()); // class
var DOMClass = /** @class */ (function () {
    function DOMClass() {}
    DOMClass.has = function (el, className) {
        if (el.classList) {
            return el.classList.contains(className);
        }
        return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    };
    DOMClass.add = function (el, className) {
        if (el.classList) {
            el.classList.add(className);
            return;
        }
        if (!this.has(el, className)) {
            el.className += " " + className;
        }
    };
    DOMClass.remove = function (el, className) {
        if (el.classList) {
            el.classList.remove(className);
            return;
        }
        if (this.has(el, className)) {
            var regex = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className = el.className.replace(regex, ' ');
        }
    };
    return DOMClass;
}()); // class
var Slider = /** @class */ (function () {
    function Slider() {
        this.id_img = 'slide-img';
        this.id_link = 'slide-link';
        this.imgs = SliderData.getImgs();
        this.loadedSlide = 1;
        this.totalSlides = this.imgs.length;
        this.urls = SliderData.getUrls();
        this.init();
    }
    Slider.prototype.activeBtn = function (el) {
        var activeClass = 'btn-active';
        DOMClass.add(el, activeClass);
    };
    Slider.prototype.getBackBtn = function () {
        return document.getElementById('back');
    };
    Slider.prototype.getLoadedSlide = function () {
        return this.loadedSlide;
    };
    Slider.prototype.getTotalSlides = function () {
        return this.totalSlides;
    };
    Slider.prototype.getNextBtn = function () {
        return document.getElementById('next');
    };
    Slider.prototype.goToBackSlide = function () {
        this.loadedSlide = this.loadedSlide - 1;
        var slide_number = this.loadedSlide - 1;
        // console.log('Slide number: ' + this.loadedSlide); // HACK: trace
        this.image.src = this.imgs[slide_number];
        this.url.href = this.urls[slide_number];
    };
    Slider.prototype.goToNextSlide = function () {
        this.loadedSlide = this.loadedSlide + 1;
        // Active "back button" in the second slide
        if (this.loadedSlide === 2) {
            this.activeBtn(this.getBackBtn());
        } else {
            // Inactive "next button" in the last slide
            if (this.loadedSlide === this.totalSlides) {
                this.inactiveBtn(this.getNextBtn());
            }
        }
        var slide_number = this.loadedSlide - 1;
        // console.log('Slide number: ' + this.loadedSlide); // HACK: trace
        this.image.src = this.imgs[slide_number];
        this.url.href = this.urls[slide_number];
    };
    Slider.prototype.inactiveBtn = function (el) {
        // console.log('Deactivating button ' + el.getAttribute('id')); // HACK: trace
        var activeClass = 'btn-active';
        DOMClass.remove(el, activeClass);
    };
    Slider.prototype.init = function () {
        // console.log('Slider / init()'); // HACK: trace
        if (this.image !== undefined) {
            // console.log('-> NO init !'); // HACK: trace
            return;
        }
        // console.log(
        //    'Inizializing slider with image "'
        //    + this.imgs[0] + '"...'); // HACK: trace
        // Load the image and link href
        this.image = document.getElementById(this.id_img);
        this.url = document.getElementById(this.id_link);
        this.image.src = this.imgs[0];
        this.url.href = this.urls[0];
        // Set "next button" active only if the slider has more than one image
        if (this.totalSlides > 1) {
            // console.log('Total slides: ' + this.totalSlides); // HACK: trace
            this.activeBtn(this.getNextBtn());
        }
    };
    return Slider;
}()); // class
window.onload = function () {
    var slider = new Slider();
    var btnBack = document.getElementById('back');
    var btnNext = document.getElementById('next');
    // Set "next button" active only if the slider has more than one image
    if (slider.getTotalSlides() > 1) {
        // console.log('Total slides: ' + slider.getTotalSlides()); // HACK: trace
        slider.activeBtn(btnNext);
    }
    btnBack.onclick = function () {
        // console.log('clicked back button'); // HACK: trace
        var slideNumber = slider.getLoadedSlide();
        var totalSlides = slider.getTotalSlides();
        // console.log('Slide: ' + slideNumber + '/' + totalSlides); // HACK: trace
        // Go to first slide -> inactive back button
        if (slideNumber === 2) {
            // console.log('Go to first slide...'); // HACK: trace
            slider.inactiveBtn(btnBack);
        } else {
            // Slide is the last -> go to back -> active next button
            if (slideNumber === totalSlides) {
                slider.activeBtn(btnNext);
            }
        }
        slider.goToBackSlide();
    }; // btnBack.onclick
    btnNext.onclick = function () {
        // console.log('clicked next button'); // HACK: trace
        var slideNumber = slider.getLoadedSlide();
        var totalSlides = slider.getTotalSlides();
        if (slideNumber < totalSlides) {
            slider.goToNextSlide();
        } else {
            //
        }
        // console.log('Slide: ' + slider.getLoadedSlide() +
        //    '/' + totalSlides); // HACK: trace
    }; // btnNext.onclick
}; // window.onload
