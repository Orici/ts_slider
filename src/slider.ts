/**
 * Test of slider with vainilla JS and CSS
 * Moisés alcocer, 2018
 * https://www.ironwoods.es
 *
 * @info: slider's js
 */
import { Slider } from './classes/Slider';

window.onload = function () {
    let slider  = new Slider();
    let btnBack = slider.getBackBtn();
    let btnNext = slider.getNextBtn();

    btnBack.onclick = function () {
        // console.log('clicked back button'); // HACK: trace

        const slideNumber = slider.getLoadedSlide();
        const totalSlides = slider.getTotalSlides();
        // console.log('Slide: ' + slideNumber + '/' + totalSlides); // HACK: trace

        // Go to first slide -> inactive back button
        if (slideNumber === 2) {
            // console.log('Go to first slide...'); // HACK: trace
            slider.inactiveBtn(btnBack);
        } else {

            // Slide is the last -> go to back -> active next button
            if (slideNumber === totalSlides) {
                slider.activeBtn(btnNext);
            }
        }

        slider.goToBackSlide();
    } // btnBack.onclick

    btnNext.onclick = function () {
        // console.log('clicked next button'); // HACK: trace

        const slideNumber = slider.getLoadedSlide();
        const totalSlides = slider.getTotalSlides();
        if (slideNumber < totalSlides) {
            slider.goToNextSlide();
        } else {
            //
        }
        // console.log(
            'Slide: ' + slider.getLoadedSlide()
            + '/' + totalSlides); // HACK: trace
    } // btnNext.onclick

} // window.onload
