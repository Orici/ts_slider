export class SliderData {

    private static IMGS = [
        './public/images/kitten_1_mini.jpg',
        './public/images/kitten_2_mini.jpg',
        './public/images/kitten_3_mini.jpg',
        './public/images/kitten_4_mini.jpg',
        './public/images/kitten_5_mini.jpg',
    ];
    private static URLS = [
        'www.mipage.com/page-1',
        'www.mipage.com/page-2',
        'www.mipage.com/page-3',
        'www.mipage.com/page-4',
        'www.mipage.com/page-5',
    ];


    public static getImgs(): string[] {
        return this.IMGS;
    }

    public static getUrls(): string[] {
        return this.URLS;
    }

} // class
