/**
 * Add and remove CSS class from the DOM
 *
 * Class methods addepted from:
 * https://stackoverflow.com/a/43824723/3919660
 */
export class DOMClass {

    public static has(el: HTMLElement, className: string): boolean {
        if (el.classList) {
            return el.classList.contains(className);
        }
        return !!el.className.match(
            new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }

    public static add(el: HTMLElement, className: string): void {
        if (el.classList) {
            el.classList.add(className)
            return;
        }

        if (!this.has(el, className)) {
            el.className += " " + className;
        }
    }

    public static remove(el: HTMLElement, className: string): void {
        if (el.classList) {
            el.classList.remove(className);
            return;
        }

        if (this.has(el, className)) {
            var regex = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className = el.className.replace(regex, ' ');
        }
    }

} // class
