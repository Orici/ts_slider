import { DOMClass } from './DOMClass';
import { SliderData } from '../data/SliderData';

export class Slider {

    private id_img: string;
    private id_link: string;
    private image: HTMLImageElement;
    private imgs: string[];
    private loadedSlide: number;
    private totalSlides: number;
    private urls: string[];
    private url: HTMLAnchorElement;


    public constructor() {
        this.id_img      = 'slide-img';
        this.id_link     = 'slide-link';
        this.imgs        = SliderData.getImgs();
        this.loadedSlide = 1;
        this.totalSlides = this.imgs.length;
        this.urls        = SliderData.getUrls();

        this.init();
    }

    public activeBtn(el: HTMLElement) {
        const activeClass = 'btn-active';
        DOMClass.add(el, activeClass);
    }

    public getBackBtn(): HTMLElement {
        return document.getElementById('back');
    }

    public getLoadedSlide(): number {
        return this.loadedSlide;
    }

    public getTotalSlides(): number {
        return this.totalSlides;
    }

    public getNextBtn(): HTMLElement {
        return document.getElementById('next');
    }

    public goToBackSlide(): void {
        this.loadedSlide = this.loadedSlide - 1;

        const slide_number = this.loadedSlide - 1;
        // console.log('Slide number: ' + this.loadedSlide); // HACK: trace
        this.image.src     = this.imgs[slide_number];
        this.url.href      = this.urls[slide_number];
    }
    public goToNextSlide(): void {
        this.loadedSlide = this.loadedSlide + 1;

        // Active "back button" in the second slide
        if (this.loadedSlide === 2) {
            this.activeBtn(this.getBackBtn());
        } else {
            // Inactive "next button" in the last slide
            if (this.loadedSlide === this.totalSlides) {
                this.inactiveBtn(this.getNextBtn());
            }
        }

        const slide_number = this.loadedSlide - 1;
        // console.log('Slide number: ' + this.loadedSlide); // HACK: trace
        this.image.src     = this.imgs[slide_number];
        this.url.href      = this.urls[slide_number];
    }

    public inactiveBtn(el: HTMLElement) {
        // console.log('Deactivating button ' + el.getAttribute('id')); // HACK: trace
        const activeClass = 'btn-active';
        DOMClass.remove(el, activeClass);
    }


    private init(): void {
        // console.log('Slider / init()'); // HACK: trace
        if (this.image !== undefined) {
            // console.log('-> NO init !'); // HACK: trace
            return;
        }
        // console.log(
        //    'Inizializing slider with image "'
        //    + this.imgs[0] + '"...'); // HACK: trace

        // Load the image and link href
        this.image = document.getElementById(this.id_img) as HTMLImageElement;
        this.url   = document.getElementById(this.id_link) as HTMLAnchorElement;
        this.image.src = this.imgs[0];
        this.url.href  = this.urls[0];

        // Set "next button" active only if the slider has more than one image
        if (this.totalSlides > 1) {
            // console.log('Total slides: ' + this.totalSlides); // HACK: trace
            this.activeBtn(this.getNextBtn());
        }
    }

} // class
